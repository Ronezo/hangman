import javax.xml.transform.stream.StreamSource;
import java.util.Scanner;
public class Main {
    static Scanner reload  = new Scanner(System.in);
    static boolean isdone = false;
    public static void main (String [] args){
        while (!isdone ){
        String word = "ronezo";
        int life = 5;
       hangman(word,life);
            System.out.println("Would you like to Play again?\n y for Yes, n for No");
            String reloadChoice = reload.nextLine();
            if (reloadChoice == "y"){
                isdone = false;
            }if (reloadChoice == "n") { isdone = true;}
        }
    }
    public static void hangman(String word, int life) {

    char [] filler = new char[word.length()];

    int i = 0;
    while (i < word.length()){ // this loop fills up the filler with '_'
        filler[i]= '_';
        if (word.charAt(i) == ' '){ // this accomodates cases where the given word has a space in between.
            filler[i]= ' ';
        }
        i++;                        // itearates through the word
    } System.out.print(filler);     // prints the empty dashes
        System.out.println("      Chances Left : "+ life); //prints the value of life
Scanner in = new Scanner (System.in);
while (life > 0){

    char guess = in.next().charAt(0); // as long as life is > 0 it waits for your input.

    if (word.contains(guess+"")) {
        for (int y= 0; y < word.length();y++){
        if (word.charAt(y) == guess){
            filler[y] = guess;       // if word contains the input at index y, the dash is replaced with that guess
        }
      }
    }else {life--;}                  // if not, life loses 1
    switch (life){

        case 1:
            System.out.println("   +----+");
            System.out.println("   O    |");
            System.out.println("  /|>   |");
            System.out.println("    )   |");
            System.out.println("        |");
            System.out.println("  [[[_]]]");
            break;

        case 2:
            System.out.println("   +----+");
            System.out.println("   O    |");
            System.out.println("  /|>   |");
            System.out.println("   |    |");
            System.out.println("        |");
            System.out.println("  [[[_]]]");
            break;

        case 3:
            System.out.println("   +----+");
            System.out.println("   O    |");
            System.out.println("   |>   |");
            System.out.println("   |    |");
            System.out.println("        |");
            System.out.println("  [[[_]]]");
            break;

        case 4:
            System.out.println("   +----+");
            System.out.println("   O    |");
            System.out.println("   |    |");
            System.out.println("   |    |");
            System.out.println("        |");
            System.out.println("  [[[_]]]");
            break;

        case 5:
            System.out.println("   +----+");
            System.out.println("   O    |");
            System.out.println("   |    |");
            System.out.println("        |");
            System.out.println("        |");
            System.out.println("  [[[_]]]");
            break;
    }

    System.out.print(filler);                          // displays the new version of filler
    System.out.println("      Chances Left : "+ life); // displays the new version of life

    if(word.equals(String.valueOf(filler))){           // if the filler equals word this runs
        System.out.println(filler);
        System.out.println(" *** You Won ***");
        System.out.println("       +----+ ");
        System.out.println("            | ");
        System.out.println("I'm free!!! | ");
        System.out.println("   O        | ");
        System.out.println("  (|/       | ");
        System.out.println("  / )  [[[_]]]");
        break;
    }

}if (life == 0){
    System.out.println("You Lose");
    System.out.println("   +----+");
    System.out.println("   O   | ");
    System.out.println("  /|>  | ");
    System.out.println("  / )  | ");
    System.out.println("       | ");
    System.out.println("  [[[_]]]");
        }

    }
}

